FROM nginx:alpine
RUN rm -rf /etc/nginx/conf.d
COPY conf /etc/nginx
COPY . /usr/share/nginx/html
EXPOSE 3369:3369
CMD ["nginx", "-g", "daemon off;"]